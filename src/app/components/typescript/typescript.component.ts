import { Component, OnInit } from '@angular/core';
// import { simpleInterface } from '../typescript/typescript.interface';
import { simpleInterface as ownNameOfInterface } from '../typescript/typescript.interface';
import * as importAllInteraces  from '../typescript/typescript.interface';

@Component({
  selector: 'app-typescript',
  templateUrl: './typescript.component.html',
  styleUrls: ['./typescript.component.scss']
})
export class TypescriptComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.methodWithInterface({
      name: 'Artur',
      age: 34
    });
  }

  methodWithInterface(args: importAllInteraces.simpleInterface): void {
    console.log(args);
  }
}
