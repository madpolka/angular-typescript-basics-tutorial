import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.scss']
})
export class ReactiveFormsComponent implements OnInit {

  genders = [
    'male',
    'female'
  ];

  // tworzysz zmienną o typie FormGroup, w której będziesz przetrzymywał obiekt formularza
  signupForm: FormGroup;


  constructor() { }

  // logikę Reactive Forms warto napisać w onInit, ponieważ jest to lifecycle hook, oraz ta metoda 
  // odpala się przed wyrenderowaniem pliku HTML
  
  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, Validators.required),
        'email': new FormControl(null, [Validators.required, Validators.email])
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([])
    });
  }

  onSubmit(): void {
    console.log(this.signupForm.valid);
    console.log(this.signupForm.value);
    
    if(!this.signupForm.valid) {
      this.signupForm.get('userData.username').markAsTouched();
      this.signupForm.get('userData.email').markAsTouched();
    } else {
      this.signupForm.reset();
    }
  }

  getControls() {
    return (this.signupForm.get('hobbies') as FormArray).controls;
  }

  onAddHobby(): void {
    const control = new FormControl(null, Validators.required);
    const formArray = this.signupForm.get('hobbies') as FormArray;
    formArray.push(control);
  }
}
